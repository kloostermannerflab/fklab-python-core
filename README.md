# Fklab - Core analysis tool

Documentation of this package can be found here: https://kloostermannerflab.bitbucket.io/


This package can be installed from the fklab conda channel.

```
conda install -c KloostermanLab fklab-python-core
```
