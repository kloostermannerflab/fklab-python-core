"""
=========================================================
Utilities for neural decoding (:mod:`fklab.decode_tools`)
=========================================================

.. currentmodule:: fklab.decode_tools

Collection of tools to help with neural decoding using
compressed kernel densities.


"""
from .maze import *
from .utilities import *

__all__ = [s for s in dir() if not s.startswith("_")]
