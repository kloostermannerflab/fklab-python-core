"""
==============================================================
Spatial neural decoding tools (:mod:`fklab.decode_tools.maze`)
==============================================================

.. currentmodule:: fklab.decode_tools.maze

Tools to prepare data for neural decoding and to evaluate
decoding performance.

"""
from fklab.version._core_version._version import __version__

__all__ = [
    "prepare_maze_for_encoding",
    "space_and_grid_from_maze",
    "spike_amp_encoding_space",
    "prepare_maze_stimulus",
    "prepare_spike_data",
    "cv_epoch_fold",
    "CVResult",
    "decode_and_compare",
    "cross_validate",
    "parallel_cross_validate",
    "analyze_decoding_error",
    "ecdf",
    "visualize_decoding_error",
    "plot_posterior",
]

import warnings
import collections
import math
import functools
import dataclasses

import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate
import seaborn as sns
import h5py

from joblib import Parallel, delayed

import fklab.segments


try:
    import compressed_kde
except ImportError:
    try:
        import fklab.decode as compressed_kde
    except ImportError:
        raise ModuleNotFoundError(name="compressed_kde")

    warnings.warn(
        "the old deprecated package of the decoding lib is used. "
        "Consider removing this one and install instead the py-compressed-kde package."
    )

import fklab.geometry.shapes as shapes
import fklab.segments
from fklab.utilities.general import inrange


def prepare_maze_for_encoding(
    maze, dx=5.0, oversampling=5, offset=None, select_position=None, per_section=True
):
    """Create space and grid for encoding/decoding.

    Parameters
    ----------
    maze : shape object
        open or closed 1d track
    dx : float, optional
        spacing of sampling grid
    oversampling : int, optional
        oversampling factor of dense discretized linear position (for closed
        tracks and graph tracks only)
    offset : int, optional
        offset into dense discretized linear position when subsampling to create
        sample grid (for closed tracks and graph tracks only)
    select_position : callable, optional
        function that returns True for linearized positions that should be
        selected.

    Returns
    -------
    data : dict
        Always contains fields: binned_track (vector of position bins on track),
        sample_grid (vector of sample points), valid (boolean vector of valid
        sample points).
        For closed tracks and graphs tracks it also includes fields:
        discrete_positions (vector of dense discretized linear positions),
        distances (distance matrix, not squared) and sample_grid_indices
        (indices into dense discretized linear positions of sample grid points)
    converter : None or callable
        function to convert linear positions to grid

    """
    data = {}
    converter = None

    if isinstance(maze, shapes.polyline):
        # construct space

        # construct grid
        pos_bins, nbins, binsize = maze.bin(dx)
        points = (pos_bins[:-1] + pos_bins[1:]) / 2.0  # bin centers

        if not select_position is None and callable(select_position):
            valid = select_position(points)
        else:
            valid = []

        data["binned_track"] = dict(position_bins=pos_bins, nbins=nbins, binsize=binsize)
        data["sample_grid"] = points
        data["valid"] = valid

    elif isinstance(
        maze, (shapes.polygon, shapes.ellipse, shapes.rectangle, shapes.graph)
    ):
        # for graph track, closed ellipse/rectangle/polygon tracks?
        # construct fine grid here, given fine grid spacing
        # compute distances here from fine grid

        # discretize position on track
        if isinstance(maze, shapes.graph):
            pos_bins, nbins, binsize = maze.bin(dx / int(oversampling), separate=True)
        else:
            pos_bins, nbins, binsize = maze.bin(dx / int(oversampling))

        oversampled_points = (pos_bins[:-1] + pos_bins[1:]) / 2.0  # position bin centers

        # compute squared distance matrix
        x, y = np.meshgrid(oversampled_points, oversampled_points)
        distances = maze.distance(x, y)

        # the sample points provided to the grid method are indices into the distance matrix
        if offset is None:
            offset = math.floor(int(oversampling) / 2)

        if per_section:
            points = []
            for nb, cn in zip(nbins, np.cumsum(np.concatenate([[0], nbins[:-1]]))):
                points.append(np.arange(offset, nb, int(oversampling)) + int(cn))
            points = np.concatenate(points).astype(np.int)
        else:
            points = np.arange(offset, len(oversampled_points), int(oversampling))

        sample_grid = oversampled_points[points]

        if not select_position is None and callable(select_position):
            valid = select_position(sample_grid)
        else:
            valid = []

        data["binned_track"] = dict(position_bins=pos_bins, nbins=nbins, binsize=binsize)
        data["discrete_positions"] = oversampled_points
        data["distances"] = distances
        data["sample_grid"] = sample_grid
        data["sample_grid_indices"] = points
        data["valid"] = valid

        converter = scipy.interpolate.interp1d(
            data["discrete_positions"],
            np.arange(len(data["discrete_positions"])),
            kind="nearest",
            bounds_error=False,
            fill_value=(0, len(data["discrete_positions"]) - 1),
        )

    elif isinstance(maze, shapes.multishape):
        raise NotImplementedError
    else:
        raise TypeError("Maze is not a valid shape object.")

    return data, converter


def space_and_grid_from_maze(data, bandwidth=5.0, direction=True, use_index=False):
    """Construct space and grid for encoding/decoding.

    Parameters
    ----------
    data : dict
        Dictionary with prepared maze data (as returned by `prepare_maze_for_encoding`)
    bandwidth : float, optional
        default kernel bandwidth for linear position
    direction : bool, optional
        include running direction in space and grid

    Returns
    -------
    space : Space object
    grid : Grid object
    """
    if not "distances" in data:
        space = compressed_kde.EuclideanSpace(["position"], bandwidth=[bandwidth])
        grid = space.grid([data["sample_grid"]], data["valid"])
    else:
        if use_index:
            space = compressed_kde.EncodedSpace(
                "position", distances=data["distances"] ** 2, bandwidth=bandwidth
            )
            grid = space.grid(data["sample_grid_indices"], data["valid"])
        else:
            space = compressed_kde.EncodedSpace(
                "position",
                points=data["discrete_positions"],
                distances=data["distances"] ** 2,
                bandwidth=bandwidth,
            )
            grid = space.grid(data["sample_grid"], data["valid"])

    if direction:
        dir_space = compressed_kde.CategoricalSpace("direction", ["inbound", "outbound"])
        dir_grid = dir_space.grid()

        space = compressed_kde.MultiSpace([space, dir_space])
        grid = space.grid([grid, dir_grid])

    return space, grid


def spike_amp_encoding_space(n=4, bandwidth=0.05):
    """Construct spike amplitude space.

    Parameters
    ----------
    n : int, optional
        number of electrodes
    bandwidth : float, optional
        default kernel bandwidth for spike amplitude dimensions

    Returns
    -------
    spike_space : Space object

    """
    # construct multi-dimensional euclidean space for spike amplitudes with default kernel bandwidth
    spike_space = compressed_kde.EuclideanSpace(
        ["amp{0}".format(x) for x in range(n)], bandwidth=[bandwidth] * n
    )

    return spike_space


def prepare_maze_stimulus(
    time,
    position,
    direction=None,
    select_position=None,
    epochs=None,
    convert_position=None,
):
    """Prepare stimulus.

    Creates stimulus array for encoding/decoding by selecting those stimuli that
    are not NaN, were measured in the desired epochs and for which the position
    meets the custom selection criteria.

    Parameters
    ----------
    time : 1d array
        stimulus time vector
    position : 1d array
        linearized position vector
    direction : 1d array, optional
        running direction vector
    select_position : callable, optional
        function that returns True for linearized positions that should be
        selected.
    epochs : (n,2) array
        time windows for stimulus selection

    Returns
    -------
    time : 1d array
        time vector for selected stimuli
    stimulus : (n,) or (n,2) array
        selected position and (optionally) direction
    epochs : (n,2) array
        final selection time windows

    """
    if len(time) != len(position) or (
        direction is not None and len(direction) != len(time)
    ):
        raise ValueError("Data vectors do not have same size")

    invalid_stimulus = np.isnan(position)

    if not select_position is None and callable(select_position):
        invalid_stimulus = np.logical_or(invalid_stimulus, ~select_position(position))
        # position[invalid_stimulus] = np.NaN
        # if not direction is None:
        #    direction[invalid_stimulus] = np.NaN

    valid_epochs = fklab.segments.Segment.fromlogical(
        ~invalid_stimulus, time, interpolate=True
    )

    if not epochs is None:
        valid_epochs.iintersection(epochs)

    selection = valid_epochs.contains(time)[0]
    # selection = slice(None,None)

    if not convert_position is None and callable(convert_position):
        position = convert_position(position)

    if direction is None:
        stimulus = position[selection]
    else:
        stimulus = np.concatenate(
            (position[selection, None], direction[selection, None]), axis=1
        )

    stim_time = time[selection]

    return stim_time, stimulus, valid_epochs


def prepare_spike_data(
    data,
    stimulus=None,
    stimulus_time=None,
    select_tt=None,
    select_amp=None,
    select_width=None,
    epochs=None,
):
    """Prepare spike data for encoding/decoding.

    Parameters
    ----------
    data : dict
        for each tetrode a nested dictionary with the time, amplitude and width
        of spikes
    stimulus : callable or (n,m) array or None
        either a function that returns the appropriate stimulus array for a spike
        time input array, or a (nspikes,nstimuli) stimulus array.
    stimulus_time : 1d array, optional
        stimulus time vector, required if stimulus is an array (will be ignored
        if stimulus is a callable)
    select_tt : list of str
        list of selected tetrodes (keys into the data dictionary)
    select_amp : scalar or [scalar, scalar], optional
        spike amplitude criteria. Either a scalar (lower threshold) or a 2-element
        list with lower and upper thresholds.
    select_width : scalar or [scalar, scalar], optional
        spike width criteria. Either a scalar (lower threshold) or a 2-element
        list with lower and upper thresholds.
    epochs : (n,2) array
        time windows for selection of spike times.

    Returns
    -------
    data : dict
        contains a nested dict for each selected tetrode with the time, amplitude
        and stimulus of selected spikes
    """
    # data should be nested dict with for each tetrode: time, amplitude, width

    # stimulus should be interpolator function or array
    if stimulus is None:
        pass
    elif not callable(stimulus):
        if stimulus_time is None:
            raise ValueError(
                "Stimulus time should be provided, if stimulus is not a interpolation function."
            )

        stimulus = scipy.interpolate.interp1d(
            stimulus_time,
            stimulus,
            axis=0,
            kind="nearest",
            bounds_error=False,
            fill_value="extrapolate",
        )

    if select_tt is None or len(select_tt) == 0:
        select_tt = list(data.keys())
    else:
        keys = list(data.keys())
        select_tt = [tt for tt in select_tt if tt in keys]

    low_amp, high_amp = _validate_selection_range(select_amp)
    low_width, high_width = _validate_selection_range(select_width)

    if epochs is None:
        epochs = fklab.segments.Segment([-np.inf, np.inf])

    selection = [
        np.logical_and.reduce(
            (
                inrange(
                    np.max(data[tt]["amplitude"], axis=1), low=low_amp, high=high_amp
                ),
                inrange(data[tt]["width"], low=low_width, high=high_width),
                epochs.contains(data[tt]["time"])[0],
            )
        )
        for tt in select_tt
    ]

    result = dict(time=collections.OrderedDict(), amplitude=collections.OrderedDict())

    if not stimulus is None:
        result["stimulus"] = collections.OrderedDict()

    for tt, sel in zip(select_tt, selection):
        result["time"][tt] = np.array(data[tt]["time"])[sel]
        result["amplitude"][tt] = np.array(data[tt]["amplitude"])[sel, :]
        if not stimulus is None:
            result["stimulus"][tt] = stimulus(result["time"][tt])

    return result


def _validate_selection_range(x):
    if x is None:
        low, high = None, None
    elif isinstance(x, (int, float)):
        low, high = float(x), None
    else:
        x = np.asarray(x).ravel()

        if len(x) == 0:
            low, high = None, None
        elif len(x) == 1:
            low, high = x[0], None
        else:
            low, high = x[:2]

    return low, high


@dataclasses.dataclass(frozen=True)
class CVResult:
    estimates: np.ndarray
    reals: np.ndarray
    errors: np.ndarray
    bins: np.ndarray
    posteriors: np.ndarray = None

    def to_hdf5(self, group):
        group.create_dataset("estimates", data=self.estimates)
        group.create_dataset("reals", data=self.reals)
        group.create_dataset("errors", data=self.errors)
        group.create_dataset("bins", data=self.bins)
        if not self.posteriors is None:
            group.create_dataset("posteriors", data=self.posteriors)

    @staticmethod
    def from_hdf5(group):
        estimates = group["estimates"][:]
        reals = group["reals"][:]
        errors = group["errors"][:]
        bins = fklab.segments.Segment(group["bins"][:])
        if "posteriors" in group:
            posteriors = group["posteriors"][:]
        else:
            posteriors = None

        return CVResult(estimates, reals, errors, bins, posteriors)

    def save_to_hdf5(self, filename, groupname):
        with h5py.File(filename, "w") as f:
            group = f.require_group(groupname)
            self.to_hdf5(group)

    @staticmethod
    def load_from_hdf5(filename, groupname):
        with h5py.File(filename, "r") as f:
            group = f[groupname]
            return CVResult.from_hdf5(group)


def cv_epoch_fold(decoder_factory, data_factory, epochs):

    all_epochs = fklab.segments.Segment(fklab.segments.segment_union(*epochs))

    for test_epochs in epochs:
        training_epochs = all_epochs.difference(test_epochs)
        yield (
            functools.partial(decoder_factory, epochs=training_epochs),
            functools.partial(data_factory, epochs=test_epochs),
        )


def _compute_map(posterior):
    # posterior should be (n,x,y,z,...) array
    # where n is the number of posteriors
    n, shape = posterior.shape[0], posterior.shape[1:]
    max_posterior = np.unravel_index(np.argmax(posterior.reshape((n, -1)), axis=1), shape)
    max_posterior = np.column_stack(max_posterior)

    return max_posterior


def decode_and_compare(decoder, data, real_stimulus_fcn, return_posterior=False):

    if callable(decoder):
        decoder = decoder()

    if callable(data):
        bins, data, binsize = data()
    else:
        bins, data, binsize = data

    # perform decoding and concatenate posteriors
    posterior = [decoder.decode(x, binsize, True) for x in data]
    posterior = np.vstack(posterior)

    # compute MAP estimates
    max_posterior = _compute_map(posterior)

    # convert indices to actual estimates
    estimates = decoder.grid().at_index(max_posterior)
    reals = real_stimulus_fcn(bins.center)
    errors = decoder.stimulus().space.distance(estimates, reals)

    return CVResult(
        estimates=estimates,
        reals=reals,
        errors=errors,
        bins=bins,
        posteriors=None if not return_posterior else posterior,
    )


def parallel_cross_validate(
    train_test_generator, real_stimulus_fcn, return_posterior=False, n_jobs=-1, **kwargs
):
    """Performs trial-fold cross-validation in parallel using joblib.

    For each fold, the decoder and data are built locally
    within the process.

    Parameters
    ----------
    train_test_generator :
        Generator for decoder_builder and data_builder functions.
    real_stimulus_fcn : callable
    return_posterior : bool
        Also return the ful posterior.
    n_jobs : int
        The number of parallel jobs to start. If -1, `n_jobs` is set equal
        to the number of CPUs. If 1, no parallel computing code is used at all,
        which is useful for debugging. For `n_jobs` below -1, (n_cpus + 1 + n_jobs)
        jobs are created. E.g., for `n_jobs` = -2, all CPUs but one are used.
    **kwargs :
        Extra arguments passed to joblib `Parallel` initializer.

    """

    job_result = Parallel(n_jobs=n_jobs, **kwargs)(
        delayed(decode_and_compare)(
            decoder,
            data,
            real_stimulus_fcn=real_stimulus_fcn,
            return_posterior=return_posterior,
        )
        for decoder, data in train_test_generator
    )

    return job_result


def cross_validate(train_test_generator, real_stimulus_fcn, return_posterior=False):

    return [
        decode_and_compare(decoder, data, real_stimulus_fcn, return_posterior)
        for decoder, data in train_test_generator
    ]


def analyze_decoding_error(cvresults, percentile, maze_nodes=None, direction=True):

    result = dict(
        position=dict(
            statistic_error=dict(),
            correct=dict(),
            statistic_error_by_section=dict(),
            correct_by_section=dict(),
            error_per_direction=dict(),
            confusion_by_section=dict(),
        )
    )

    if direction:
        result.update(
            dict(
                direction=dict(
                    correct=dict(),
                    correct_by_section=dict(),
                    correct_per_direction=dict(),
                )
            )
        )

    for t, cvresult in enumerate(cvresults):
        # compute error statistics
        result["position"]["statistic_error"][t] = np.percentile(
            cvresult.errors[:, 0], percentile
        )

        if not maze_nodes is None:
            nsections = len(maze_nodes) - 1
            estimated_section = np.digitize(cvresult.estimates[:, 0], maze_nodes) - 1
            real_section = np.digitize(cvresult.reals[:, 0], maze_nodes) - 1

            result["position"]["correct"][t] = 100.0 * np.mean(
                estimated_section == real_section
            )

            tmp1 = []
            tmp2 = []
            # compute error statistics per maze section
            for k in range(nsections):
                section_errors = cvresult.errors[
                    np.logical_and(
                        cvresult.reals[:, 0] >= maze_nodes[k],
                        cvresult.reals[:, 0] < maze_nodes[k + 1],
                    ),
                    0,
                ]
                if len(section_errors) == 0:
                    tmp1.append(np.nan)
                else:
                    tmp1.append(np.percentile(section_errors, percentile))

                section_correct = estimated_section[real_section == k] == k
                if len(section_correct) == 0:
                    tmp2.append(np.nan)
                else:
                    tmp2.append(100.0 * np.mean(section_correct))

            result["position"]["statistic_error_by_section"][t] = tmp1
            result["position"]["correct_by_section"][t] = tmp2

            # compute confusion matrix for maze sections: P(estimate|real)
            m = np.histogram2d(
                cvresult.reals[:, 0], cvresult.estimates[:, 0], bins=maze_nodes
            )[0]
            m = m / np.sum(m, axis=1, keepdims=True)
            result["position"]["confusion_by_section"][t] = m

            if direction:
                if cvresult.errors.ndim > 1 and cvresult.errors.shape[1] > 1:
                    result["direction"]["correct"][t] = dict(
                        correct=100 * np.mean(cvresult.errors[:, 1] == 0)
                    )

    estimates = np.concatenate([x.estimates for x in cvresults], axis=0)
    reals = np.concatenate([x.reals for x in cvresults], axis=0)
    errors = np.concatenate([x.errors for x in cvresults], axis=0)

    # compute error statistics
    result["position"]["statistic_error"]["all"] = np.percentile(errors[:, 0], percentile)

    if not maze_nodes is None:
        nsections = len(maze_nodes) - 1
        estimated_section = np.digitize(estimates[:, 0], maze_nodes) - 1
        real_section = np.digitize(reals[:, 0], maze_nodes) - 1
        result["position"]["correct"]["all"] = 100.0 * np.mean(
            estimated_section == real_section
        )

        tmp1 = []
        tmp2 = []

        # compute error statistics per maze section
        for k in range(nsections):
            section_errors = errors[
                np.logical_and(
                    reals[:, 0] >= maze_nodes[k], reals[:, 0] < maze_nodes[k + 1]
                ),
                0,
            ]
            if len(section_errors) == 0:
                tmp1.append(np.nan)
            else:
                tmp1.append(np.percentile(section_errors, percentile))
            section_correct = estimated_section[real_section == k] == k
            if len(section_correct) == 0:
                tmp2.append(np.nan)
            else:
                tmp2.append(100.0 * np.mean(section_correct))
        result["position"]["statistic_error_by_section"]["all"] = tmp1
        result["position"]["correct_by_section"]["all"] = tmp2

        # compute confusion matrix for maze sections: P(estimate|real)
        m = np.histogram2d(reals[:, 0], estimates[:, 0], bins=maze_nodes)[0]
        m = m / np.sum(m, axis=1, keepdims=True)
        result["position"]["confusion_by_section"]["all"] = m

        if direction:
            for d in [0, 1]:
                section_errors = errors[reals[:, 1] == d, 0]
                result["position"]["error_per_direction"][d] = np.percentile(
                    section_errors, percentile
                )

    if direction:
        if errors.ndim > 1 and errors.shape[1] > 1:
            result["direction"]["correct"]["all"] = dict(
                correct=100 * np.mean(errors[:, 1] == 0)
            )

            tmp1 = []
            for k in range(nsections):
                section_errors = errors[
                    np.logical_and(
                        reals[:, 0] >= maze_nodes[k], reals[:, 0] < maze_nodes[k + 1]
                    ),
                    1,
                ]
                if len(section_errors) == 0:
                    tmp1.append(np.nan)
                else:
                    tmp1.append(100 * np.mean(section_errors == 0))
            result["direction"]["correct_by_section"]["all"] = tmp1

            for d in [0, 1]:
                section_errors = errors[reals[:, 1] == d, 1]
                result["direction"]["correct_per_direction"][d] = 100 * np.mean(
                    section_errors == 0
                )

    return result


def ecdf(x):
    return np.sort(x), np.arange(1, len(x) + 1, dtype=np.float) / len(x)


def visualize_decoding_error(
    cvresults, percentile, maze_nodes=None, direction=True, square_heatmap=True
):

    stats = analyze_decoding_error(cvresults, percentile, maze_nodes, direction=direction)

    estimates = np.concatenate([x.estimates for x in cvresults], axis=0)
    reals = np.concatenate([x.reals for x in cvresults], axis=0)
    errors = np.concatenate([x.errors for x in cvresults], axis=0)

    ncols = 2 if maze_nodes is None else 3

    # plot confusion matrix: true position vs estimated position
    fig, ax = plt.subplots(1, ncols, figsize=(ncols * 4, 4))

    # plot maze section borders
    if not maze_nodes is None:
        for val in maze_nodes:
            ax[0].axhline(val, color="thistle", linewidth=0.5)
            ax[0].axvline(val, color="thistle", linewidth=0.5)

    ax[0].plot(
        reals[:, 0],
        estimates[:, 0],
        "ko",
        alpha=0.3,
        markersize=4,
        markeredgecolor="none",
    )
    ax[0].set_ylabel("estimated position on track [linearized cm]")
    ax[0].set_xlabel("real position on track [linearized cm]")
    ax[0].set_title("Confusion plot")

    # plot error distribution
    x, y = ecdf(errors[:, 0])

    ax[1].plot(x, y, "k")
    ax[1].set_ylabel("cumulative density")
    ax[1].set_xlabel("error [cm]")
    ax[1].set_title("Error distribution")
    ax[1].plot(stats["position"]["statistic_error"]["all"], percentile / 100, "ko")
    ax[1].text(
        stats["position"]["statistic_error"]["all"] + 10,
        percentile / 100.0,
        "error = {0:0.1f}cm".format(stats["position"]["statistic_error"]["all"]),
    )

    # plot confusion matrix by maze section
    if not maze_nodes is None:
        if square_heatmap:
            sns.heatmap(
                stats["position"]["confusion_by_section"]["all"].T,
                annot=True,
                fmt="0.2f",
                linewidths=0.5,
                vmin=0,
                vmax=1,
                ax=ax[2],
                annot_kws=dict(size="xx-small"),
                xticklabels=range(1, len(maze_nodes)),
                yticklabels=range(1, len(maze_nodes)),
            )
            ax[2].invert_yaxis()
            cbar = ax[2].collections[0].colorbar
            cbar.set_label("P(estimate|real)")
        else:
            hmesh = ax[2].pcolormesh(
                maze_nodes,
                maze_nodes,
                stats["position"]["confusion_by_section"]["all"].T,
                cmap="magma",
                vmin=0,
                vmax=1,
                edgecolors="white",
            )
            plt.colorbar(hmesh, ax=ax[2], label="P(estimate|real)")

        ax[2].set(xlabel="real section", ylabel="estimated section")
        ax[2].set_title("Confusion (maze section)")

    return fig, ax


def _color_posterior(x):

    if x.ndim < 3:
        return x
    elif x.ndim > 3 or x.shape[2] != 2:
        raise ValueError("Expecting a 3d array of shape (m,n,2).")

    result = np.zeros(x.shape[:2] + (3,))

    s = np.sum(x, axis=2)
    d = x[:, :, 1] - x[:, :, 0]

    # from red to black to blue
    # result[:,:,0] = 1 - (d.clip(-1,0) + 1)*s
    # result[:,:,1] = 1-s
    # result[:,:,2] = 1 + (d.clip(0,1) - 1)*s

    # from red to blue (as in Davidson et al., 2009)
    result[:, :, 0] = 1 - s * (d + 1) / 2
    result[:, :, 1] = 1 - s
    result[:, :, 2] = 1 + s * (d - 1) / 2

    return result.transpose([1, 0, 2])


def plot_posterior(
    posterior,
    x=None,
    y=None,
    colorbar=False,
    xlabel="",
    ylabel="",
    ax=None,
    scale=None,
    cmap="viridis",
):

    if ax is None:
        fig, ax = plt.subplots(1, 1)
    else:
        fig = ax.get_figure()

    if x is None:
        x = (0, posterior.shape[0])
    else:
        x = np.asarray(x).ravel()
        x = (x[0], x[-1])

    if y is None:
        y = (0, posterior.shape[1])
    else:
        y = np.asarray(y).ravel()
        y = (y[0], y[-1])

    if scale is None:
        scale = (0, 1)
    else:
        scale = np.atleast_1d(scale)
        if len(scale) == 0:
            scale = (0, 2)
        elif len(scale) == 1:
            scale = (0, float(scale[0]))
        else:
            scale = (scale[0], scale[-1])

    img = ax.imshow(
        _color_posterior(posterior),
        aspect="auto",
        origin="lower",
        extent=x + y,
        vmin=scale[0],
        vmax=scale[1],
        cmap=cmap,
        label="posterior",
    )
    ax.set(xlabel=xlabel, ylabel=ylabel)

    if colorbar:
        plt.colorbar(img, label="posterior probability")

    return fig, ax, img
