"""
===========================================================
Neural decoding tools (:mod:`fklab.decode_tools.utilities`)
===========================================================

.. currentmodule:: fklab.decode_tools.utilities

Factory functions to build decoders and to prepare data for decoding.

"""
from fklab.version._core_version._version import __version__

__all__ = ["build_decoder", "decoder_factory", "build_data", "data_factory"]

import warnings

from functools import partial
from functools import wraps

import numpy as np

import fklab.segments

try:
    import compressed_kde.decode
    from compressed_kde.compressed_kde import Grid
    from compressed_kde.compressed_kde import Space
    from compressed_kde.decode import Decoder
    from compressed_kde.decode import PoissonLikelihood
    from compressed_kde.decode import Stimulus
except ImportError:
    try:
        import fklab.decode
        from fklab.decode import Decoder
        from fklab.decode import Grid
        from fklab.decode import PoissonLikelihood
        from fklab.decode import Space
        from fklab.decode import Stimulus
    except ImportError:
        raise ModuleNotFoundError(name="compressed_kde.decode")

    warnings.warn(
        "the old deprecated package of the decoding lib is used. "
        "Consider removing this one and install instead the py-compressed-kde package."
    )


def build_decoder(
    stimulus_space=None,
    stimulus_grid=None,
    stimulus_duration=None,
    stimulus=None,
    stimulus_time=None,
    event_space=None,
    event_time=None,
    event_stimulus=None,
    event_data=None,
    compression=1.0,
    epochs=None,
    rate_scale=1.0,
    random=True,
    select_sources=None,
):
    """Use as an helper function to build a decoder object.

    Parameters
    ----------
    compression : float, optional
    epochs : segment array, optional
    stimulus_space : Space object, required
    stimulus_grid : Grid object, required
    stimulus_duration : float, required
    stimulus_time : float, optional
    stimulus : array, required
    event_space : Space object (or list of Space objects??), optional
    event_time : list of arrays, optional
    event_stimulus : list of arrays, required
    event_data : list of arrays, optional
    rate_scale : float
    select_sources : list of booleans, optional

    Returns
    -------
    Decoder object

    """
    # epochs and stimulus_time can be omitted
    # if epochs is specified, then stimulus_time is required
    # stimulus, stimulus_space, stimulus_grid, stimulus_duration are required
    # compression is optional (default=1)
    # event_time can be omitted
    # if epochs is specified, then event_time is required
    # event_stimulus is required
    # event_space and event_data can be omitted
    # event_space and event_data are both required as soon as one is specified

    if not isinstance(stimulus_space, Space) or not isinstance(stimulus_grid, Grid):
        raise TypeError("Invalid stimulus space and/or stimulus grid.")

    if not epochs is None and stimulus_time is None:
        raise ValueError("Stimulus time is required, if epochs are provided.")

    # first create a Stimulus object that represents the stimulus probability density
    stim = Stimulus(stimulus_space, stimulus_grid, stimulus_duration, compression)
    stim.random_insertion = random

    if epochs is None:
        stim.add_stimuli(stimulus)
    else:
        stim.add_stimuli(stimulus[epochs.contains(stimulus_time)[0]])

    # then build the likelihoods for each of the sources
    if not epochs is None and event_time is None:
        raise ValueError("Event time is required, if epochs are provided.")

    if (not event_space is None and event_data is None) or (
        event_space is None and not event_data is None
    ):
        raise ValueError(
            "Event data and event space should be either both omitted or both specified."
        )

    likelihoods = []

    if event_space is None:
        for et, es in zip(event_time, event_stimulus):

            if epochs is None:
                selection = slice(None)
            else:
                selection = epochs.contains(et)[0]

            L = PoissonLikelihood(stim)
            L.random_insertion = random
            L.add_events(es[selection])
            L.rate_scale = rate_scale
            L.precompute()
            likelihoods.append(L)
    else:
        for et, es, ed in zip(event_time, event_stimulus, event_data):

            if epochs is None:
                selection = slice(None)
            else:
                selection = epochs.contains(et)[0]

            L = PoissonLikelihood(event_space, stim)
            L.random_insertion = random
            L.add_events(np.concatenate((ed[selection], es[selection]), axis=1))
            L.rate_scale = rate_scale
            likelihoods.append(L)

    # build decoder from likelihoods
    decoder = Decoder(likelihoods, [])

    if select_sources is not None:
        decoder.enable_sources(select_sources)

    return decoder


@wraps(build_decoder)
def decoder_factory(**kwargs):
    return partial(build_decoder, **kwargs)


def build_data(
    event_time=None,
    event_data=None,
    decode_bin=None,
    epochs=None,
    overlap=None,
    fraction=None,
):
    """Use as a helper function to prepare data for decoding.

    Parameters
    ----------
    event_time: list of 1d arrays, required
        spike time vectors
    event_data: list of (n,m) arrays, optional
        spike feature arrays
    decode_bin: float, optional
        decoding bin size
    epochs: (n,2) array, required
        time epochs for which to perform decoding
    overlap: float, optional
        define range of overlap between decoding bins
    fraction: float, optional
        to define decoding bins as a fraction of epochs

    Returns
    -------
    segments : (n,2) array
        time bins for decoding
    data : nested list of (n,m) arrays
        binned spike features

    """
    # epochs and decode_bin are required
    # event_time is required
    # event_data is optional
    epochs = fklab.segments.Segment(epochs)

    # then build the likelihoods for each of the sources
    if not decode_bin is None and not fraction is None:
        raise ValueError("Cannot use both decode_bin and fraction to define bin size")

    # split epochs into smaller bins for decoding
    if decode_bin is not None:
        if overlap is not None:
            decode_segments = epochs.split(size=decode_bin, overlap=overlap)
        else:
            decode_segments = epochs.split(size=decode_bin)
    if fraction is not None:
        decode_segments = fklab.segments.Segment(
            np.concatenate(
                [
                    epochs[i].split(size=epochs[i].duration / fraction)
                    for i in range(len(epochs))
                ]
            )
        )

    bins = [decode_segments.contains(x, expand=True)[2] for x in event_time]

    # extract event data for each decoding bin
    if event_data is None:
        data = [
            [
                np.zeros((0, 0))
                if len(b[s]) == 1 and b[s][0] == -1
                else np.zeros((len(b[s]), 0))
                for b in bins
            ]
            for s in range(len(decode_segments))
        ]
    else:
        data = [
            [
                np.zeros((0, x.shape[1]))
                if len(b[s]) == 1 and b[s][0] == -1
                else x[b[s], :]
                for x, b in zip(event_data, bins)
            ]
            for s in range(len(decode_segments))
        ]

    return decode_segments, data, decode_bin


@wraps(build_data)
def data_factory(**kwargs):
    return partial(build_data, **kwargs)
