"""
=============================================
Ripple analysis (:mod:`fklab.signals.ripple`)
=============================================

.. currentmodule:: fklab.signals.ripple

Ripple analysis.


"""
from .ripple import *


__all__ = [s for s in dir() if not s.startswith("_")]
