"""
====================================
Ripple (:mod:`fklab.signals.ripple`)
====================================

.. currentmodule:: fklab.signals.ripple

Utilities for detection and analysis of hippocampal ripples

"""
import collections

import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1
import numpy as np
import scipy.signal
import seaborn as sns

import fklab.events
import fklab.plot
import fklab.segments
import fklab.signals.core
import fklab.signals.filter
import fklab.signals.multitaper
import fklab.statistics.core


__all__ = [
    "ripple_envelope",
    "detect_ripples",
    "evaluate_online_ripple_detection",
    "analyze_ripples",
    "ripple_modulation_metric",
    "compute_ripple_modulation",
    "visualize_ripple_features",
    "visualize_ripple_envelopes",
    "visualize_ripple_spectra",
]


def ripple_envelope(signals, **kwargs):

    smooth_options = dict(kernel="gaussian", bandwidth=0.0075)
    smooth_options.update(kwargs.pop("smooth_options", {}))

    filter_options = dict(transition_width="25%", attenuation=60)
    filter_options.update(kwargs.pop("filter_options", {}))

    return fklab.signals.filter.compute_envelope(
        signals,
        "ripple",
        filter_options=filter_options,
        smooth_options=smooth_options,
        **kwargs
    )


def detect_ripples(
    time,
    signals,
    axis=-1,
    isenvelope=False,
    isfiltered=False,
    segments=None,
    threshold=None,
    threshold_kind="median",
    allowable_gap=0.02,
    minimum_duration=0.03,
    peak_detection_options=None,
    filter_options={},
    smooth_options={},
):
    """Detect ripple events.

    Parameters
    ----------
    time : 1D array
    signal : array
        either an array of raw signals (isenvelope==False and isfiltered==False), or an array of already filtered signals
        (isenvelope==False, isfiltered==True) or a 1D array with pre-computed envelope (isenvelope==True)
    axis : scalar, optional
        axis of the time dimension in the signal array (not used if signal is already a pre-computed envelope)
    isenvelope : bool, optional
    isfiltered : bool, optional
    segments : segment-like
        restrict detection of ripples to segments
    threshold : scalar, 2-element sequence or callable, optional
        single upper threshold or [lower, upper] threshold for ripple detection. If a callable, the function should accept a 1D array
        and return a 1 or 2-element sequence of thresholds, computed from the input array
    threshold_kind : {'raw', 'median', 'zscore','percentile'}
    allowable_gap : scalar, optional
        minimum gap between adjacent ripples. If the start of a ripple
        is within allowable_gap of a the end of previous ripple, then the two ripple events are merged.
    minimum_duration : scalar, optional
        minimum duration of a ripple event. Shorter duration events are discarded.
    peak_detection_options : dict, optional
        dictionary with extra options for local peak detection. Default: {"method": "gradient", "interp": "quadratic"}.
    filter_options : dict, optional
        dictionary with options for filtering (if signal is not already filtered)
    smooth_options : dict, optional
        dictionary with options for envelope smoothing (if envelope was not pre-computed).

    Returns
    -------
    (peak_time, peak_amp) : 1D arrays
        time and amplitude of ripple peaks above upper threshold
    segments : Segment
        ripple start and end times
    (low,high) : scalars
        lower and upper threshold used for ripple detection

    See Also
    --------
    fklab.signals.core.localmaxima
    apply_filter
    apply_median_filter
    construct_filter
    ripple_envelope
    """
    dt = np.median(np.diff(time))

    if not isenvelope:
        envelope = ripple_envelope(
            signals,
            axis=axis,
            fs=1.0 / dt,
            isfiltered=isfiltered,
            filter_options=filter_options,
            smooth_options=smooth_options,
        )
    else:
        envelope = signals
        if envelope.ndim != 1:
            raise ValueError("Envelope needs to be a vector.")

    # compute signal statistics in segments
    # and determine thresholds
    if segments is None:
        segments = [-np.inf, np.inf]

    segments = fklab.segments.check_segments(segments)

    threshold = fklab.signals.core.compute_threshold(
        time, envelope, threshold, segments=segments, kind=threshold_kind
    )

    low, high = threshold[0], threshold[-1]

    # find ripple peaks
    if peak_detection_options is None:
        peak_detection_options = {"method": "gradient", "interp": "quadratic"}
    elif not isinstance(peak_detection_options, dict):
        raise ValueError("Expecting a dict for peak_detection_options.")

    ripple_peak_time, ripple_peak_amp = fklab.signals.core.localmaxima(
        envelope, x=time, yrange=[high, np.inf], **peak_detection_options
    )

    # find bumps
    ripple_segments = fklab.signals.core.detect_mountains(
        envelope,
        x=time,
        low=low,
        high=high,
        segments=segments,
        allowable_gap=allowable_gap,
        minimum_duration=minimum_duration,
    )

    # only retain ripple peaks within segments
    selection = ripple_segments.contains(ripple_peak_time)[0]
    ripple_peak_time = ripple_peak_time[selection]
    ripple_peak_amp = ripple_peak_amp[selection]

    return (ripple_peak_time, ripple_peak_amp), ripple_segments, (low, high)


def evaluate_online_ripple_detection(
    online_ripple, offline_ripple, window=0.25, lock_out=None
):
    """Evaluate online ripple detection accuracy.

    Parameters
    ----------
    online_ripple : 1d array
        Times of online detected ripples
    offline_ripple : Segment
        Time segments for offline detected ripples
    window : scalar
        Analysis window.
    lock_out : None or scalar
        Lock-out period after online detected ripple

    """
    n_online = len(online_ripple)
    n_offline = len(offline_ripple)

    lags = np.linspace(0, window, np.ceil(window / 0.005))
    peth_offline, lags = fklab.events.peri_event_histogram(
        online_ripple, reference=offline_ripple.start, lags=lags
    )

    interval_to_offline, idx = fklab.events.event_intervals(
        online_ripple, offline_ripple.start, "pre"
    )

    true_positive = np.sum(-interval_to_offline < offline_ripple.duration[idx])
    true_positive_rate = 100.0 * true_positive / n_offline

    false_negative = n_offline - true_positive
    false_negative_rate = 100.0 * false_negative / n_offline

    false_positive = n_online - true_positive
    false_positive_rate = 100.0 * false_positive / n_online

    if lock_out is not None:
        interval_to_online, _ = fklab.events.event_intervals(
            offline_ripple.start, online_ripple, "pre"
        )
        n_lock_out = np.sum(-interval_to_online < lock_out)
        lock_out_rate = 100.0 * n_lock_out / n_offline

    # plotting
    fig = plt.figure()
    host = mpl_toolkits.axes_grid1.host_subplot(111)

    # plot distribution of online detction latencies
    bar = host.bar(
        left=1000 * lags[:, 0],
        bottom=0,
        width=1000 * np.diff(lags, axis=1),
        height=peth_offline,
        label="delay of online detected events",
    )
    l = host.set_ylabel("number of online events")
    l.set_color(bar.patches[0].get_facecolor())
    host.set_xlabel("time [ms] from offline ripple start")

    # plot offline ripple event durations
    par = host.twinx()
    xx = np.linspace(0, window, 100)
    yy = fklab.segments.segment_count(offline_ripple - offline_ripple.start, xx)
    par.plot(
        1000 * xx, yy, color="black", label="# offline ripple events of certain duration"
    )
    par.grid(False)
    par.set_ylabel("number of offline events")

    # display statistics
    txt = "{tprate:.1f}% ({tp}/{n}) of ripple events detected online".format(
        tprate=true_positive_rate, tp=true_positive, n=n_offline
    )
    txt += "\n{fnrate:.1f}% ({fn}/{n}) ripple events missed".format(
        fnrate=false_negative_rate, fn=false_negative, n=n_offline
    )

    if lock_out is not None:
        txt += "\n{r:.1f}% ({nlock}/{n}) in lock out period ({p:.1f} ms)".format(
            r=lock_out_rate, nlock=n_lock_out, n=n_offline, p=1000 * lock_out
        )

    txt += "\n{fprate:.1f}% ({fp}/{n}) spurious events detected online".format(
        fprate=false_positive_rate, fp=false_positive, n=n_online
    )

    txtbox = plt.text(
        0.95, 0.95, txt, transform=host.transAxes, va="top", ha="right", linespacing=2
    )

    host.legend(loc="right")

    host.set_xlim(0, window * 1000)


def analyze_ripples(
    t,
    signal,
    fs=None,
    segments=None,
    freq_band="ripple",
    filter_kw={},
    smooth_bandwidth=0.02,
    detection_kw={},
    spectrum_kw={},
    window_size=0.25,
):
    """Detect and analyze ripple events.

    Parameters
    ----------
    t : 1d-array
        Time vector.
    signal : 1d-array or list of 1d-arrays
        Wide-band signal(s).
    fs : None or float
        Sampling frequency of the signal(s). If None,
        then the sampling frequency is determined from
        the time vector.
    segments : (n,2) array or Segment
        Time segments to restrict ripple detection to.
    freq_band : str or (float, float)
        Lower and upper cut-off frequency for ripple
        band-pass filtering.
    filter_kw : dict
        Extra keyword arguments for `apply_filter` function.
    smooth_bandwidth : float
        Bandwidth for gaussian smoothing of envelope.
    detection_kw : dict
        Extra keyword arguments for `detect_ripples` function. Valid
        keywords (and default) are: threshold ([1,5]), allowable_gap
        (0.03), and minimum_duration (0.03).
    spectrum_kw : dict
        Extra keyword arguments for computing spectrum. Valid keywords
        (and default) are: window_size (0.1), bandwidth (10) and fpass ([100,250]).
    window_size : float

    Returns
    -------
    dict
        A dictionary with the following key/values:
        * n, number of detected ripples
        * peaks, peaks in ripple envelope
        * peak_times, times of peaks in ripple envelope
        * ripple_windows, ripple start and end times
        * threshold, detection threshold in data units
        * npeaks, the number of peaks per ripple
        * max_peak, largest peak per ripple
        * max_peak_time, time of largest peak per ripple
        * max_peak_rel_time, time of largest peak per ripple relative to center time
        * ripple_signals, unfiltered signal snippet around ripples
        * ripple_filtered, filtered signal snippet around ripples
        * ripple_envelope, envelope snippet around ripples
        * ripple_t, time for snippets
        * spectra, spectral density per ripple
        * frequency, frequency vector for spectra
        * peak_frequency, peak frequency in spectra
        * peak_power, peak power in spectra
        * options, dictionary with options

    """

    if fs is None:
        fs = 1.0 / np.min(np.diff(t))

    if not isinstance(signal, (tuple, list)):
        signal = [signal]

    _detection_kw = {"threshold": [1, 5], "allowable_gap": 0.03, "minimum_duration": 0.03}

    _spectrum_kw = {"window_size": 0.1, "bandwidth": 10, "fpass": [100, 250]}

    _detection_kw.update(**detection_kw)
    _spectrum_kw.update(**spectrum_kw)

    # filter signal
    filtered_signal = [
        fklab.signals.filter.apply_filter(sig, freq_band, fs=fs, **filter_kw)
        for sig in signal
    ]

    # compute envelope
    envelope = ripple_envelope(
        filtered_signal,
        smooth_options={"bandwidth": smooth_bandwidth},
        fs=fs,
        isfiltered=True,
    )

    # detect ripples in envelope
    (ripple_peak_time, ripple_peak), ripple_windows, abs_threshold = detect_ripples(
        t, envelope, isenvelope=True, **_detection_kw
    )

    # find maximum ripple peak and number of ripple peaks
    _, npeaks, idx = ripple_windows.contains(ripple_peak_time)

    max_peak_time = []
    max_peak_rel_time = []
    max_peak = []

    for k, c in zip(idx, ripple_windows.center):
        if k[0] == -1:
            max_peak_time.append(np.nan)
            max_peak_rel_time.append(np.nan)
            max_peak.append(np.nan)
        else:
            maxpeak_idx = k[0] + np.argmax(ripple_peak[k[0] : k[1] + 1])
            max_peak_time.append(ripple_peak_time[maxpeak_idx])
            max_peak_rel_time.append(ripple_peak_time[maxpeak_idx] - c)
            max_peak.append(ripple_peak[maxpeak_idx])

    max_peak_time = np.array(max_peak_time)
    max_peak_rel_time = np.array(max_peak_rel_time)
    max_peak = np.array(max_peak)

    # extract ripple windows
    if len(ripple_windows) > 0:

        _, ripple_signals = fklab.signals.core.extract_trigger_windows(
            signal[0], ripple_windows.center, window=window_size, fs=fs, start_time=t[0]
        )

        _, ripple_filtered = fklab.signals.core.extract_trigger_windows(
            filtered_signal[0],
            ripple_windows.center,
            window=window_size,
            fs=fs,
            start_time=t[0],
        )

        _, ripple_envelopes = fklab.signals.core.extract_trigger_windows(
            envelope, ripple_windows.center, window=window_size, fs=fs, start_time=t[0]
        )

        window_t = (np.arange(len(ripple_signals)) - (len(ripple_signals) - 1) / 2) / fs

        # compute spectra
        if _spectrum_kw["window_size"] == window_size:
            y = ripple_signals
        else:
            _, y = fklab.signals.core.extract_trigger_windows(
                signal[0],
                ripple_windows.center,
                window=_spectrum_kw["window_size"],
                fs=fs,
                start_time=t[0],
            )

        spectra, freq, _, _ = fklab.signals.multitaper.multitaper._mtspectrum_single(
            y,
            fs=fs,
            bandwidth=_spectrum_kw["bandwidth"],
            error="none",
            fpass=_spectrum_kw["fpass"],
        )

        # compute peak frequency and power
        peak_idx = np.argmax(spectra * freq[:, None], axis=0)
        peak_freq = freq[peak_idx]
        peak_power = spectra[peak_idx, range(spectra.shape[1])]

    else:
        ripple_signals = None
        ripple_filtered = None
        ripple_envelopes = None
        window_t = None
        spectra = None
        freq = None
        peak_freq = None
        peak_power = None

    return dict(
        n=len(ripple_windows),  # number of detected ripples
        peaks=ripple_peak,  # peaks in ripple envelope
        peak_times=ripple_peak_time,  # times of peaks in ripple envelope
        ripple_windows=ripple_windows,  # ripple start and end times
        threshold=abs_threshold,  # detection threshold in data units
        npeaks=npeaks,  # the number of peaks per ripple
        max_peak=max_peak,  # largest peak per ripple
        max_peak_time=max_peak_time,  # time of largest peak per ripple
        max_peak_rel_time=max_peak_rel_time,  # time of largest peak per ripple relative to center time
        ripple_signals=ripple_signals,  # unfiltered signal snippet around ripples
        ripple_filtered=ripple_filtered,  # filtered signal snippet around ripples
        ripple_envelopes=ripple_envelopes,  # envelope snippet around ripples
        ripple_t=window_t,  # time for snippets
        spectra=spectra,  # spectral density per ripple
        frequency=freq,  # frequency vector for spectra
        peak_frequency=peak_freq,  # peak frequency in spectra
        peak_power=peak_power,  # peak power in spectra
        options=dict(  # options dictionary
            smooth_bandwidth=smooth_bandwidth,
            window_size=window_size,
            fs=fs,
            detection=_detection_kw,
            spectrum=_spectrum_kw,
            filter=filter_kw,
            freq_band=freq_band,
        ),
    )


def ripple_modulation_metric(n_in, n_ref=None, duration=None, metric="rate"):
    """Compute ripple modulation metric for spike counts.

    Parameters
    ----------
    n_in : 1d-array like
        Number of spikes inside ripples.
    n_ref : None or 1d-array like
        Number of spikes in reference time window.
        Required for metrics: 'relative_rate', 'relative_fraction',
        'depth', 'relative_depth', 'normalized'.
    duration : None or 1d-array like
        Ripple durations in seconds, Required for metrics:
        'relative_rate', 'rate'.
    metric : str
        A valid metric, one of: 'rate', 'relative_rate', 'fraction',
        'relative_fraction', 'depth', 'relative_depth', 'normalized'.

    Returns
    -------
    float
        Ripple modulation metric.

    """

    n_in = np.atleast_1d(n_in).ravel()

    n = len(n_in)

    if n_ref is None and kind in [
        "relative_rate",
        "relative_fraction",
        "depth",
        "relative_depth",
        "normalized",
    ]:
        raise ValueError("Number of reference spikes is required for {}.".format(kind))

    if duration is None and kind in ["relative_rate", "rate"]:
        raise ValueError("Duration is required for {}.".format(kind))

    if metric == "rate":
        m = np.sum(n_in) / np.sum(duration)
    elif metric == "relative_rate":
        m = (np.sum(n_in) - np.sum(n_ref)) / np.sum(duration)
    elif metric == "fraction":
        m = np.sum(n_in > 0) / n
    elif metric == "relative_fraction":
        m = (np.sum(n_in > 0) - np.sum(n_ref > 0)) / n
    elif metric == "depth":
        m = (np.sum(n_in) - np.sum(n_ref)) / n
    elif metric == "relative_depth":
        m = (np.sum(n_in) - np.sum(n_ref)) / np.sum(n_ref)
    elif metric == "normalized":
        m = (np.sum(n_in) - np.sum(n_ref)) / (np.sum(n_in) + np.sum(n_ref))
    else:
        raise ValueError("Unknown modulation metric.")

    return m


def compute_ripple_modulation(
    ripple_windows, spikes, metric="rate", reference="pre", offset=0, alpha=0.01
):
    """Compute ripple modulation metric for spike trains.

    Parameters
    ----------
    ripple_windows : (n,2) or Segment
    spikes : 1d-array like or list of 1d-array like
    metric : str
        Valid ripple modulation metric. See `ripple_modulation_metric`.
    reference : 'pre', 'post' or 'both'
        Which time windows to use as reference.
    offset : float
        Time offset of reference windows.
    alpha : float
        Alpha level for confidence intervals.

    Returns
    -------
    metric : 1d-array
        Vector of ripple modulation metrics.
    ci : array
        Array with confidence intervals.


    """

    ripple_windows = fklab.segments.Segment(ripple_windows)

    if not isinstance(spikes, (tuple, list)):
        spikes = [spikes]

    spikes = [np.atleast_1d(k).ravel() for k in spikes]
    spikes = [x[np.isfinite(x)] for x in spikes]

    nspikes_in_ripple = [ripple_windows.contains(sp)[1] for sp in spikes]

    offset = float(offset)
    alpha = float(alpha)

    nspikes_ref = [np.zeros(len(ripple_windows))] * len(spikes)

    if reference == "pre" or reference == "both":
        nspikes_ref = [
            ref + (ripple_windows - ripple_windows.duration - offset).contains(sp)[1]
            for sp, ref in zip(spikes, nspikes_ref)
        ]

    if reference == "post" or reference == "both":
        nspikes_ref = [
            ref + (ripple_windows + ripple_windows.duration + offset).contains(sp)[1]
            for sp, ref in zip(spikes, nspikes_ref)
        ]

    if reference == "both":
        nspikes_ref = [k / 2 for k in nspikes_ref]

    m = [
        ripple_modulation_metric(n, nref, duration=ripple_windows.duration, metric=metric)
        for n, nref in zip(nspikes_in_ripple, nspikes_ref)
    ]

    m_ci = [
        fklab.statistics.core.ci(
            np.column_stack([nspikes, nref, ripple_windows.duration]),
            statistic=lambda k: ripple_modulation_metric(
                k[:, 0], k[:, 1], k[:, 2], metric=metric
            ),
            alpha=alpha,
        )
        for nspikes, nref in zip(nspikes_in_ripple, nspikes_ref)
    ]

    return np.array(m), np.array(m_ci)


def visualize_ripple_features(
    ripple_data,
    units="μV",
    color="k",
    marker_size=5,
    alpha=1,
    show_threshold=True,
    show_minimum_duration=True,
):
    """Plot ripple amplitude and duration.

    Parameters
    ----------
    ripple_data : dict
        Result of the `analyze_ripples` function.
    units : str
        Signal units.

    Returns
    -------
    Seaborn JointGrid object

    """

    if ripple_data["n"] == 0:
        return None

    valid = np.isfinite(ripple_data["max_peak"])

    jg = sns.jointplot(
        x=ripple_data["ripple_windows"][valid].duration * 1000,
        y=ripple_data["max_peak"][valid],
        kind="scatter",
        s=marker_size,
        alpha=alpha,
        color=color,
        joint_kws=dict(
            color=None,
            c=plt.get_cmap("tab10")((ripple_data["npeaks"][valid] > 1).astype(int)),
        ),
    )

    jg.ax_joint.set_xlim(0, None)
    jg.ax_joint.set_ylim(0, None)

    jg.set_axis_labels(xlabel="duration [ms]", ylabel="peak amplitude [{}]".format(units))

    if show_threshold:
        fklab.plot.labeled_hmarker(
            ripple_data["threshold"][1],
            "upper threshold",
            ax=jg.ax_joint,
            text_x=1,
            text_offset=1,
            style={"ls": "-", "color": "lightgray"},
        )

        if ripple_data["threshold"][0] < ripple_data["threshold"][1]:
            fklab.plot.labeled_hmarker(
                ripple_data["threshold"][0],
                "lower threshold",
                ax=jg.ax_joint,
                text_x=1,
                text_offset=1,
                style={"ls": "-", "color": "lightgray"},
            )

    if show_minimum_duration:
        mindur = ripple_data["options"]["detection"]["minimum_duration"] * 1000
        if mindur > 0:
            fklab.plot.labeled_vmarker(
                mindur,
                "minimum duration",
                ax=jg.ax_joint,
                text_y=1,
                text_offset=1,
                style={"ls": "-", "color": "lightgray"},
            )

    return jg


def visualize_ripple_envelopes(ripple_data, units="μV", axes=None):
    """Plot ripple envelopes.

    Parameters
    ----------
    ripple_data : dict
        Result of the `analyze_ripples` function.
    units : str
        Signal units.
    axes : None, dict or list of Axes
        Axes for plotting can be specified as a dictionary with
        keys for each of the three plot types: envelopes,
        ripple_windows and ripple_peaks. If a list of axes is
        provided, then they are mapped to the three plot types
        in the order above. If None, a new figure with three axes
        will be created.

    Returns
    -------
    axes : dict

    """

    if ripple_data["n"] == 0:
        return None

    if axes is None:
        fig, axes = plt.subplots(
            3, 1, sharex=True, figsize=(5, 8), gridspec_kw=dict(height_ratios=[3, 1, 1])
        )
        axes = {"envelopes": axes[0], "ripple_windows": axes[1], "ripple_peaks": axes[2]}
    elif isinstance(axes, collections.abc.Iterable):
        axes = {
            k: v for k, v in zip(["envelopes", "ripple_windows", "ripple_peaks"], axes)
        }
    elif not isinstance(axes, collections.abc.Mapping):
        axes = {"envelopes": axes}

    mindur = ripple_data["options"]["detection"]["minimum_duration"] * 1000
    xx = ripple_data["ripple_t"] * 1000  # ms

    if "envelopes" in axes:
        axes["envelopes"].axhline(
            ripple_data["threshold"][1], color="orange", lw=1, ls="--"
        )
        axes["envelopes"].axhline(
            ripple_data["threshold"][0], color="orange", lw=1, ls="--"
        )

        axes["envelopes"].axvline(-mindur / 2, color="orange", lw=1, ls="--")
        axes["envelopes"].axvline(mindur / 2, color="orange", lw=1, ls="--")

        axes["envelopes"].plot(xx, ripple_data["ripple_envelopes"], color="k", alpha=0.05)
        axes["envelopes"].plot(
            xx, np.mean(ripple_data["ripple_envelopes"], axis=1), "white"
        )

        axes["envelopes"].set(ylim=(0, None), ylabel="amplitude [{}]".format(units))
        axes["envelopes"].text(
            0.98,
            0.95,
            "ripple\nenvelope",
            va="top",
            ha="right",
            transform=axes["envelopes"].transAxes,
        )

    if "ripple_windows" in axes:
        axes["ripple_windows"].fill_between(
            xx,
            fklab.segments.segment_count(
                (ripple_data["ripple_windows"] - ripple_data["ripple_windows"].center)
                * 1000,
                xx,
            )
            / ripple_data["n"],
            edgecolor="k",
            facecolor=[0.9, 0.9, 0.9],
        )

        axes["ripple_windows"].axvline(-mindur / 2, color="orange", lw=1, ls="--")
        axes["ripple_windows"].axvline(mindur / 2, color="orange", lw=1, ls="--")
        axes["ripple_windows"].set(ylim=(0, None), ylabel="fraction")

        axes["ripple_windows"].text(
            0.98,
            0.95,
            "distribution of\nripple window sizes",
            va="top",
            ha="right",
            transform=axes["ripple_windows"].transAxes,
        )

    if "ripple_peaks" in axes:
        sns.kdeplot(
            x=ripple_data["max_peak_rel_time"] * 1000,
            ax=axes["ripple_peaks"],
            color="k",
            fill=True,
            facecolor=[0.9, 0.9, 0.9],
            lw=1,
            alpha=1,
        )

        axes["ripple_peaks"].axvline(-mindur / 2, color="orange", lw=1, ls="--")
        axes["ripple_peaks"].axvline(mindur / 2, color="orange", lw=1, ls="--")

        axes["ripple_peaks"].text(
            0.98,
            0.95,
            "distribution\nof ripple peaks",
            va="top",
            ha="right",
            transform=axes["ripple_peaks"].transAxes,
        )

        axes["ripple_peaks"].set(
            xlabel="time relative to center of ripple window [ms]",
            xlim=xx[[0, -1]],
            ylabel="density",
        )

    return axes


def visualize_ripple_spectra(ripple_data, units="μV", axes=None):
    """Plot ripple spectra.

    Parameters
    ----------
    ripple_data : dict
        Result of the `analyze_ripples` function.
    units : str
        Signal units.
    axes : None, dict or list of Axes
        Axes for plotting can be specified as a dictionary with
        keys for each of the three plot types: envelopes,
        ripple_windows and ripple_peaks. If a list of axes is
        provided, then they are mapped to the three plot types
        in the order above. If None, a new figure with three axes
        will be created.

    Returns
    -------
    axes : dict

    """

    if ripple_data["n"] == 0:
        return None

    if axes is None:
        fig, axes = plt.subplots(
            1, 2, figsize=(15, 3), gridspec_kw=dict(width_ratios=[10, 1]), sharey=True
        )
        axes = {"spectra": axes[0], "peak_frequency": axes[1]}
    elif isinstance(axes, collections.abc.Iterable):
        axes = {k: v for k, v in zip(["spectra", "peak_frequency"], axes)}
    elif not isinstance(axes, collections.abc.Mapping):
        axes = {"spectra": axes}

    order = np.argsort(ripple_data["peak_frequency"])
    df = np.mean(np.diff(ripple_data["frequency"]))
    img = axes["spectra"].imshow(
        ripple_data["spectra"][:, order]
        / np.max(ripple_data["spectra"][:, order], axis=0, keepdims=True),
        aspect="auto",
        origin="lower",
        extent=[
            0.5,
            ripple_data["n"] + 0.5,
            ripple_data["frequency"][0] - 0.5 * df,
            ripple_data["frequency"][-1] + 0.5 * df,
        ],
        cmap="inferno",
    )

    sns.histplot(y=ripple_data["peak_frequency"], ax=axes["peak_frequency"], color="k")

    axes["peak_frequency"].axhline(140)
    axes["peak_frequency"].axhline(220)

    axes["spectra"].set(ylim=(ripple_data["frequency"][[0, -1]]), ylabel="frequency [Hz]")

    return axes
